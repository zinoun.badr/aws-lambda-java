package com.serverless;

import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.serverless.dal.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeleteProductHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {

        try {
            // get the 'pathParameters' from input
            Map<String, String> pathParameters = (Map<String, String>) input.get("pathParameters");
            String productId = pathParameters.get("id");

            // get the Product by id
            Boolean success = new Product().delete(productId);

            // send the response back
            if (success) {
                return ApiGatewayResponse.builder().setStatusCode(204).build();
            } else {
                return ApiGatewayResponse.builder().setStatusCode(404)
                        .setObjectBody("Product with id: '" + productId + "' not found.").build();
            }
        } catch (Exception ex) {
            logger.error("Error in deleting product: " + ex);

            Response responseBody = new Response("Error in deleting product: ", input);
            return ApiGatewayResponse.builder()
                    .setStatusCode(500)
                    .setObjectBody(responseBody)
                    .build();


        }
    }
}