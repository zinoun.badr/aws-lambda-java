package com.serverless;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.serverless.dal.Product;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ListProductHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

	private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
        try {
            // get all products
            List<Product> products = new Product().list();

            // send the response back
            return ApiGatewayResponse.builder().setStatusCode(200).setObjectBody(products).build();
        } catch (Exception ex) {
            logger.error("Error in listing products: " + ex);

            Response responseBody = new Response("Error in Listing product: ", input);
            return ApiGatewayResponse.builder().setStatusCode(500).setObjectBody(responseBody).build();
        }

    }
}
