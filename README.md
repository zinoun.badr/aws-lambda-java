### WIP  
- [x] Create Java Project ( Serverless Template)
- [x] Add Rest End point 
- [x] Describe ressource , Role , Gatway in serverless.yml 
- [x] Locally Deploy / Remove
- [ ] Add CI/CD pipeline 
- [ ] Add Static Code analysis (Sonar)
- [ ] Dev Sec Ops ?

[logo]: ./aws_arch.png 
![alt text](aws_arch.png)


# Aws Lambda REST API in Java/Maven & DynamoDB deployed with Serverless framework (SLS)

The sample serverless service will create a REST API for products. It will be deployed to AWS. The data will be stored in a DynamoDB table.

## Install Pre-requisites

* `node` 
* `npm`
* `maven >= 3.5 `
* `JDK 8`
* `serverless` 
  
## Build the Java project

Create the java artifact (jar) by:

``` shell
$ cd `SOURCE_PATH`
$ mvn clean install
```
We can see that we have an artifact in the `target` folder named `products-api-dev.jar`.

## Deploy the serverless app

``` shell
$ sls deploy
```

## Test the API

Let's invoke each of the four functions that we created as part of the app.

### Create Product

``` shell 
$ curl -X POST https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products -d '{"name": "Product10", "price": 99.99}'

{"id":"ba04f16b-f346-4b54-9884-957c3dff8c0d","name":"Product10","price":99.99}
```

### List Products

```
$ curl https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products

[{"id":"dfe41235-0fe5-4e6f-9a9a-19b7b7ee79eb","name":"Product3","price":7.49},
{"id":"ba04f16b-f346-4b54-9884-957c3dff8c0d","name":"Product1","price":9.99},
{"id":"6db3efe0-f45c-4c5f-a73c-541a4857ae1d","name":"Product4","price":2.69},
{"id":"370015f8-a8b9-4498-bfe8-f005dbbb501f","name":"Product2","price":5.99},
{"id":"cb097196-d659-4ba5-b6b3-ead4c07a8428","name":"Product5","price":15.49}]
```

**No Product(s) Found:**

```
$ curl https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products

[]
```

### Get Product

```
$ curl https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products/ba04f16b-f346-4b54-9884-957c3dff8c0d

{"id":"ba04f16b-f346-4b54-9884-957c3dff8c0d","name":"Product1","price":9.99}
```

**Product Not Found:**

```
curl https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products/xxxx

"Product with id: 'xxxx' not found."
```

### DeleteProduct

```
$ curl -X DELETE https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products/24ada348-07e8-4414-8a8f-7903a6cb0253
```

**Product Not Found:**

```
curl -X DELETE https://$$$$$$$.execute-api.us-east-1.amazonaws.com/dev/products/xxxx

"Product with id: 'xxxx' not found."
```

## View the CloudWatch Logs

```
$ serverless logs --function getProduct
```

## View the Metrics

View the metrics for the service:

```
$ serverless metrics
```

Or, view the metrics for only one function:

```
$ serverless metrics --function hello

```